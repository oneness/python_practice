#!/usr/bin/env python
# -*- coding: utf-8 -*-

from BeautifulSoup import BeautifulSoup
import re
import requests

URL = 'https://docs.openshift.org/latest/rest_api/openshift_v1.html'
re = requests.get(URL, verify=False)
html = re.content
soup = BeautifulSoup(''.join(html))

tmp = soup.findAll('div', 'sectionbody')

soup = BeautifulSoup(''.join(str(tmp[2])))

api_div_list = soup.findAll('div', 'sect2')


fd = open('1.md', 'a+')
for one in api_div_list:
    a_soup = BeautifulSoup(''.join(str(one)))
    fd.write( '[' + a_soup.h3.text + ']' + '(' + URL + a_soup.h3.a.attrs[1][1]  + ')' + '\n\n')
#    fd.write('```' + '\n')
#    fd.write(a_soup.pre.text + '\n')
#    fd.write('```' + '\n\n')

fd.close()